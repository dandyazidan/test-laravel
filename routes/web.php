<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/register', function () {
//     return view('register');
// });

// Route::get('/login', function () {
//     return view('login');
// });


Route::get('/register', 'AuthController@getRegister')->name('register')->middleware('guest');
Route::post('/register', 'AuthController@postRegister')->middleware('guest');
Route::get('/login', 'AuthController@getLogin')->name('login')->middleware('guest');
Route::post('/login', 'AuthController@postLogin')->middleware('guest');

Route::get('/logout', 'AuthController@logout')->name('logout')->middleware('auth');

Route::get('/dashboard', function () {
    return view('admin.dashboard');
})->name('dashboard')->middleware('auth');

Route::get('/produk', 'ProdukController@getProduk')->name('produk')->middleware('auth');
Route::get('/add-produk', 'ProdukController@addProduk');
Route::post('/produk', 'ProdukController@saveProduk');

Route::get('/ekspedisi', function () {
    return view('admin.ekspedisi');
})->name('ekspedisi')->middleware('auth');

Route::post('/ekspedisi', 'EkspedisiController@storeData');
Route::get('/order', 'OrderController@getOrder')->name('order');
Route::post('/order', 'OrderController@addOrder');
Route::get('/add-order', 'OrderController@getAddOrder')->name('add order')->middleware('auth');
Route::get('/updateStatus', 'EkspedisiController@updateStatus');
