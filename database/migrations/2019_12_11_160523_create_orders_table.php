<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_id');
            $table->string('sku_id');
            $table->string('pop_sku_id');
            $table->string('quantity');
            $table->string('pay_type');
            $table->string('booktime')->nullable();
            $table->string('total_price');
            $table->string('installment_fee')->nullable();
            $table->string('order_state')->nullable();
            $table->string('freight')->nullable();
            $table->string('customer_name');
            $table->string('remark')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('order_complete_time')->nullable();
            $table->string('province');
            $table->string('city');
            $table->string('district');
            $table->string('address');
            $table->string('phone');
            $table->string('delivery_type');
            $table->string('carrier_code')->nullable();
            $table->string('express_company')->nullable();
            $table->string('promotion_amount')->nullable();
            $table->string('coupon_amount')->nullable();
            $table->string('pay_sub_total');
            $table->string('taxation_amount')->nullable();
            $table->string('self_delivery_id')->nullable();
            $table->string('status_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
