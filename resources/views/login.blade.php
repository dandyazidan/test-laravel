@include('template.auth.header')
<div class="login-box">
    <div class="login-logo">
        <a href="index2.html"><strong>Challenge</strong>Test</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form method="POST" action="{{route('login')}}">
            {{csrf_field()}}
            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="Email" name="email" id="email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-control">
                <button type="submit" class="btn btn-primary btn-raised btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
        </form>

        <!-- <a href="#">I forgot my password</a><br> -->
        <br>
        <hr>
        <div class="text-center">
            <a href="/register" class="text-center">Sign Up</a>
        </div>


    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@include('template.auth.footer')