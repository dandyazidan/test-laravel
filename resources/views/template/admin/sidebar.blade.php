<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{url('assets/admin/dist/img/user-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <!-- <li>
                <a href="">
                    <i class="fa fa-th"></i> <span>Pelanggan</span>
                    <span class="pull-right-container"></span>
                </a>
            </li> -->

            <li>
                <a href="{{url('/produk')}}">
                    <i class="fa fa-th"></i> <span>Product</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>

            <li>
                <a href="{{url('/order')}}">
                    <i class="fa  fa-reorder"></i> <span>Order</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>

            <li>
                <a href="{{url('/ekspedisi')}}">
                    <i class="fa  fa-file-excel-o"></i> <span>Ekspedisi</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- 
            <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>