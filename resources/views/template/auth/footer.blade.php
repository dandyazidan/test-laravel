<!-- jQuery 3 -->
<script src="{{url('assets/admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{url('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Material Design -->
<script src="{{url('assets/admin/dist/js/material.min.js')}}"></script>
<script src="{{url('assets/admin/dist/js/ripples.min.js')}}"></script>
<script>
    $.material.init();
</script>
<!-- iCheck -->
<!-- <script src="plugins/iCheck/icheck.min.js')}}"></script> 
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });-->
</script>
</body>

</html>