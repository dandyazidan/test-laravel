@include('template.admin.head')

<div class="wrapper">
  @include('template.admin.header')

  @include('template.admin.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
    <!-- Content Header (Page header) -->

  </div>
  @include('template.admin.footer')


  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
@include('template.admin.script')