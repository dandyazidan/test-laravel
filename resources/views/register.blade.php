@include('template.auth.header')
<div class="login-box">
    <div class="login-logo">
        <a href="index2.html"><strong>Challenge</strong>Test</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form method="POST" action="{{route('register')}}">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="Nama Lengkap" name="name" id="name">
                @if ($errors->has('name'))
                <div class="invalid-feedback">
                    {{ $errors->first('name')}}
                </div>
                @endif
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Email" name="email" id="email" value="{{old('email')}}">
                @if ($errors->has('email'))
                <div class="invalid-feedback">
                    {{ $errors->first('email')}}
                </div>
                @endif
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Password" name="password" id="password">
                @if ($errors->has('password'))
                <div class="invalid-feedback">
                    {{ $errors->first('password')}}
                </div>
                @endif
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control{{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" placeholder="Confirm Password" name="password_confirmation" id="confirm_password">
                @if ($errors->has('password_confirmation'))
                <div class="invalid-feedback">
                    {{ $errors->first('password_confirmation')}}
                </div>
                @endif
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-control">
                <button type="submit" class="btn btn-primary btn-raised btn-block btn-flat">Sign Up</button>
            </div>
            <!-- /.col -->
        </form>

        <!-- <a href="#">I forgot my password</a><br> -->
        <br>
        <hr>
        <div class="text-center">
            <a href="/login" class="text-center">Sign In</a>
        </div>


    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@include('template.auth.footer')