@extends('template.default-admin')
@section('title', 'Order')
@section('content')
<section class="content-header">
    <h1>
        Produk
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Order</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }} <br>
            </div>
            @endif

            @if (session('error'))
            <div class="alert alert-success">
                {{ session('error') }}
            </div>
            @endif
            <a href="{{url('/add-order')}}" class="btn btn-primary btn-xs bg-teal">
                <li class="fa fa-plus"></li> Add New Order
            </a>
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Order ID</th>
                        <th>SKU ID</th>
                        <th>POP SKU ID</th>
                        <th>Customer Name</th>
                        <th>Status </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $orders as $o)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $o->order_id }}</td>
                        <td>{{ $o->sku_id }}</td>
                        <td>{{ $o->pop_sku_id }}</td>
                        <td>{{ $o->customer_name }}</td>
                        <td>{{ $o->status_order }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
@endsection