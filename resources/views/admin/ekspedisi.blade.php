@extends('template.default-admin')
@section('title', 'Ekspedisi')
@section('content')
<section class="content-header">
    <h1>
        Produk
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header with-border col-mb-0">
            <h3 class="box-title">Ekspedisi</h3>
        </div>
        <div class="box-body">
            <form action="{{ url('/ekspedisi') }}" method="post" enctype="multipart/form-data">
                @csrf

                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }} <br>
                    <a href="{{url('/order')}}"><strong>Klik disini!</strong>Untuk melihat status order</a>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-success">
                    {{ session('error') }}
                </div>
                @endif
                <label>Upload file (.xls, .xlsx)</label>
                <div class="input-group input-group-sm">
                    <input type="file" class="upload" name="file">
                    <p class="text-danger">{{ $errors->first('file') }}</p>
                    <span class="input-group-btn">
                        <button class="btn btn-primary bg-teal btn-sm">Upload</button>
                    </span>
                </div>
            </form>
        </div>
        <div class="box-footer">
            <p><strong>*Note : </strong>unggah file excel untuk melihat status order</p>
        </div>
    </div>
    <!-- <div class="row" style="padding-top: 30px">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('/ekspedisi') }}" method="post" enctype="multipart/form-data">


                        <div class="form-group">
                            <label class="custom-file-label bg-yellow btn btn-primary btn-sm" for="">
                                <h5>Choose file (.xls, .xlsx)</h5>
                            </label>
                            <input type="file" class="upload" name="file">
                            <p class="text-danger">{{ $errors->first('file') }}</p>
                        </div>

                        <button class="btn btn-primary bg-navy btn-lg">Upload</button>

                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6"></div>
    </div> -->
</section>
@endsection