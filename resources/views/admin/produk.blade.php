@extends('template.default-admin')
@section('title', 'Porduk')
@section('content')
<section class="content-header">
    <h1>
        Produk
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Produk</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }} <br>
            </div>
            @endif

            @if (session('error'))
            <div class="alert alert-success">
                {{ session('error') }}
            </div>
            @endif
            <a href="{{url('/add-produk')}}" class="btn btn-primary btn-xs bg-teal">
                <li class="fa fa-plus"></li> Add New Produk
            </a>
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>SKU ID</th>
                        <th>POP SKU ID</th>
                        <th>Harga</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $produk as $p)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $p->sku_id }}</td>
                        <td>{{ $p->pop_sku_id }}</td>
                        <td>{{ $p->harga }}</td>
                        <td>
                            <a href="#" class="btn btn-primary bg-yellow btn-xs">Edit</a>
                            <a href="#" class="btn btn-primary bg-red btn-xs">Hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection