@extends('template.default-admin')
@section('title', 'Add Order')
@section('content')
<section class="content-header">
    <h1>
        Data Order
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Add New Data Order</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <form method="POST" action="{{url('/order')}}">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="order_id">Order ID*</label>
                    <input type="text" class="form-control {{ $errors->has('order_id') ? 'is-invalid' : '' }} " id=" order_id" name="order_id" placeholder="Order ID">
                    @if ($errors->has('order_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('order_id')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label>SKU ID*</label>
                    <select class="form-control {{ $errors->has('sku_id') ? 'is-invalid' : '' }}" name=" sku_id">
                        <option>Pilih Produk</option>
                        @foreach($produk as $pr)
                        <option value="{{$pr->sku_id}}">{{$pr->sku_id ." - ". $pr->pop_sku_id}}</option>
                        @endforeach
                        @if ($errors->has('sku_id'))
                        <div class="invalid-feedback">
                            {{ $errors->first('sku_id')}}
                        </div>
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label for="pop_sku_id">POP SKU ID*</label>
                    <input type="text" class="form-control {{ $errors->has('pop_sku_id') ? 'is-invalid' : '' }}" id=" pop_sku_id" placeholder="POP SKU ID" name="pop_sku_id">
                    @if ($errors->has('pop_sku_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('pop_sku_id')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="quantity">Quantity*</label>
                    <input type="text" class="form-control {{ $errors->has('quantity') ? 'is-invalid' : '' }}" id=" quantity" placeholder="Quantity" name="quantity">
                    @if ($errors->has('quantity'))
                    <div class="invalid-feedback">
                        {{ $errors->first('quantity')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="pay_type">Pay Type*</label>
                    <input type="text" class="form-control {{ $errors->has('pay_type') ? 'is-invalid' : '' }}" id=" pay_type" placeholder="Pay Type" name="pay_type">
                    @if ($errors->has('pay_type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('pay_type')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="booktime">Booktime</label>
                    <input type="text" class="form-control {{ $errors->has('booktime') ? 'is-invalid' : '' }}" id=" booktime" placeholder="Booktime" name="booktime">
                    @if ($errors->has('booktime'))
                    <div class="invalid-feedback">
                        {{ $errors->first('booktime')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="total_price">Total Orice*</label>
                    <input type="text" class="form-control {{ $errors->has('total_price') ? 'is-invalid' : '' }}" id=" total_price" placeholder="Total Price" name="total_price">
                    @if ($errors->has('total_price'))
                    <div class="invalid-feedback">
                        {{ $errors->first('total_price')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="installment_fee">Installment Fee</label>
                    <input type="text" class="form-control {{ $errors->has('installment_fee') ? 'is-invalid' : '' }}" id=" installment_fee" placeholder="Installment Fee" name="installment_fee">
                    @if ($errors->has('installment_fee'))
                    <div class="invalid-feedback">
                        {{ $errors->first('installment_fee')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="order_state">Order State</label>
                    <input type="text" class="form-control {{ $errors->has('order_state') ? 'is-invalid' : '' }}" id=" order_state" placeholder="Order State" name="order_state">
                    @if ($errors->has('order_state'))
                    <div class="invalid-feedback">
                        {{ $errors->first('order_state')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="freight">Freight</label>
                    <input type="text" class="form-control {{ $errors->has('freight') ? 'is-invalid' : '' }}" id=" freight" placeholder="Freight" name="freight">
                    @if ($errors->has('freight'))
                    <div class="invalid-feedback">
                        {{ $errors->first('freight')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="customer_name">Customer Name*</label>
                    <input type="text" class="form-control {{ $errors->has('costomer_name') ? 'is-invalid' : '' }}" id=" customer_name" placeholder="Customer Name" name="customer_name">
                    @if ($errors->has('customer_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('customer_name')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="remark">Remark</label>
                    <input type="text" class="form-control {{ $errors->has('remark') ? 'is-invalid' : '' }}" id=" remark" placeholder="Remark" name="remark">
                    @if ($errors->has('remark'))
                    <div class="invalid-feedback">
                        {{ $errors->first('remark')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="delivery_time">Delivery Time</label>
                    <input type="text" class="form-control {{ $errors->has('delivery_time') ? 'is-invalid' : '' }}" id=" delivery" placeholder="Delivery" name="delivery_time">
                    @if ($errors->has('delivery_time'))
                    <div class="invalid-feedback">
                        {{ $errors->first('delivery_time')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="order_complete_time">Order Complete Time</label>
                    <input type="text" class="form-control {{ $errors->has('order_complete_time') ? 'is-invalid' : '' }}" id=" order_complete_time" placeholder="Order Complete Time" name="order_complete_time">
                    @if ($errors->has('order_complete_time'))
                    <div class="invalid-feedback">
                        {{ $errors->first('order_complete_time')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="province">Province*</label>
                    <input type="text" class="form-control {{ $errors->has('province') ? 'is-invalid' : '' }}" id=" province" placeholder="Province" name="province">
                    @if ($errors->has('province'))
                    <div class="invalid-feedback">
                        {{ $errors->first('province')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="city">City*</label>
                    <input type="text" class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}" id=" city" placeholder="City" name="city">
                    @if ($errors->has('city'))
                    <div class="invalid-feedback">
                        {{ $errors->first('city')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="district">District*</label>
                    <input type="text" class="form-control {{ $errors->has('district') ? 'is-invalid' : '' }}" id=" district" placeholder="District" name="district">
                    @if ($errors->has('district'))
                    <div class="invalid-feedback">
                        {{ $errors->first('district')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="address">Address*</label>
                    <input type="text" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" id=" address" placeholder="Address" name="address">
                    @if ($errors->has('address'))
                    <div class="invalid-feedback">
                        {{ $errors->first('address')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="phone">Phone*</label>
                    <input type="text" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" id=" phone" placeholder="Phone" name="phone">
                    @if ($errors->has('phone'))
                    <div class="invalid-feedback">
                        {{ $errors->first('phone')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="delivery_type">Delivery Type*</label>
                    <input type="text" class="form-control {{ $errors->has('delivery_type') ? 'is-invalid' : '' }}" id=" delivery_type" placeholder="Delivery Type" name="delivery_type">
                    @if ($errors->has('delivery_type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('delivery_type')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="carrier_code">Carrier Code</label>
                    <input type="text" class="form-control {{ $errors->has('carrier_code') ? 'is-invalid' : '' }}" id=" carrier_code" placeholder="Carrier Code" name="carrier_code">
                    @if ($errors->has('carrier_code'))
                    <div class="invalid-feedback">
                        {{ $errors->first('carrier_code')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="express_company">Express Company</label>
                    <input type="text" class="form-control {{ $errors->has('express_company') ? 'is-invalid' : '' }}" id=" express_company" placeholder="Express Company" name="express_company">
                    @if ($errors->has('express_company'))
                    <div class="invalid-feedback">
                        {{ $errors->first('express_company')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="promotion_amount">Promotion Amount</label>
                    <input type="text" class="form-control {{ $errors->has('promotion_amount') ? 'is-invalid' : '' }}" id=" promotion_amount" placeholder="Promotion Amount" name="promotion_amount">
                    @if ($errors->has('promotion_amount'))
                    <div class="invalid-feedback">
                        {{ $errors->first('promotion_amount')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="coupon_amount">Coupon Amount</label>
                    <input type="text" class="form-control {{ $errors->has('coupon_amount') ? 'is-invalid' : '' }}" id=" coupon_amount" placeholder="Coupon Amount" name="coupon_amount">
                    @if ($errors->has('coupon_amount'))
                    <div class="invalid-feedback">
                        {{ $errors->first('coupon_amount')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="pay_sub_total">Pay Sub Total*</label>
                    <input type="text" class="form-control {{ $errors->has('pay_sub_total') ? 'is-invalid' : '' }}" id=" pay_sub_total" placeholder="Pay Sub Total" name="pay_sub_total">
                    @if ($errors->has('pay_sub_total'))
                    <div class="invalid-feedback">
                        {{ $errors->first('pay_sub_total')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="taxation_amount">Taxation Amount</label>
                    <input type="text" class="form-control {{ $errors->has('taxation_amount') ? 'is-invalid' : '' }}" id=" taxation_amount" placeholder="Taxation Amount" name="taxation_amount">
                    @if ($errors->has('taxation_amount'))
                    <div class="invalid-feedback">
                        {{ $errors->first('taxation_amount')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="self_delivery_id">Self Delivery Id</label>
                    <input type="text" class="form-control {{ $errors->has('selft_delivery_id') ? 'is-invalid' : '' }}" id=" self_delivery_id" placeholder="Self Delivery ID" name="self_delivery_id">
                    @if ($errors->has('self_delivery_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('self_delivery_id')}}
                    </div>
                    @endif
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary bg-teal">Simpan</button>
            </div>
        </form>
    </div>
    <!-- /.box -->
</section>
@endsection
<script type="text/javascript">
    $("#pilihProduk").on("change", function() {

        // ambil nilai
        var sku_id = $("#pilihProduk option:selected").attr("sku_id");
        var pop_sku_id = $("#pilihProduk option:selected").attr("pop_sku_id");
        // pindahkan nilai ke input
        $("#sku_id").val(sku_id);
        $("#pop_sku_id").val(pop_sku_id);
    });
</script>