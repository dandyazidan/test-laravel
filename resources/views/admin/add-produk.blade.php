@extends('template.default-admin')
@section('title', 'Add Produk')
@section('content')
<section class="content-header">
    <h1>
        Data Produk
        <!-- <small>Control panel</small> -->
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Add New Produk</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <form method="POST" action="{{url('/produk')}}">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="sku_id">SKU ID*</label>
                    <input type="text" class="form-control {{ $errors->has('sku_id') ? 'is-invalid' : '' }} " id="sku_id" name="sku_id" placeholder="SKU ID">
                    @if ($errors->has('sku_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sku_id')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="pop_sku_id">POP SKU ID</label>
                    <input type="text" class="form-control {{ $errors->has('pop_sku_id') ? 'is-invalid' : '' }}" id=" taxation_amount" placeholder="POP SKU ID" name="pop_sku_id">
                    @if ($errors->has('pop_sku_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('pop_sku_id')}}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control {{ $errors->has('harga') ? 'is-invalid' : '' }}" id="harga" placeholder="Harga" name="harga">
                    @if ($errors->has('harga'))
                    <div class="invalid-feedback">
                        {{ $errors->first('harga')}}
                    </div>
                    @endif
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary bg-teal">Simpan</button>
            </div>
        </form>
    </div>
    <!-- /.box -->
</section>
@endsection
<script type="text/javascript">
    $("#pilihProduk").on("change", function() {

        // ambil nilai
        var sku_id = $("#pilihProduk option:selected").attr("sku_id");
        var pop_sku_id = $("#pilihProduk option:selected").attr("pop_sku_id");
        // pindahkan nilai ke input
        $("#sku_id").val(sku_id);
        $("#pop_sku_id").val(pop_sku_id);
    });
</script>