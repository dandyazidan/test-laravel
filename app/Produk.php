<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    //
    protected $table = 'produk';
    protected $fillable = [
        'sku_id',
        'pop_sku_id',
        'harga'
    ];
}
