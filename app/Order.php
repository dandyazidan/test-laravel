<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $fillable = [
        'order_id',
        'sku_id',
        'pop_sku_id',
        'quantity',
        'pay_type',
        'booktime',
        'total_price',
        'installment_fee',
        'order_state',
        'freight',
        'customer_name',
        'remark',
        'delivery_time',
        'order_complete_time',
        'province',
        'city',
        'district',
        'address',
        'phone',
        'delivery_type',
        'carrier_code',
        'express_company',
        'promotion_amount',
        'coupon_amount',
        'pay_sub_total',
        'taxation_amount',
        'self_delivery_id',
        'status_order'
    ];
}
