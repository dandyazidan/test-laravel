<?php

namespace App\Imports;

use App\Ekspedisi;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EkspedisiImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Ekspedisi([
            'order_id'              => $row['orderid'],
            'sku_id'                => $row['sku_id'],
            'pop_sku_id'            => $row['pop_sku_id'],
            'quantity'              => $row['quantity'],
            'pay_type'              => $row['pay_type'],
            'booktime'              => $row['booktime'],
            'total_price'           => $row['total_price'],
            'installment_fee'       => $row['installment_fee'],
            'order_state'           => $row['order_state'],
            'freight'               => $row['freight'],
            'customer_name'         => $row['customer_name'],
            'remark'                => $row['remark'],
            'delivery_time'         => $row['delivery_time'],
            'order_complete_time'   => $row['order_complete_time'],
            'province'              => $row['province'],
            'city'                  => $row['city'],
            'district'              => $row['district'],
            'address'               => $row['address'],
            'phone'                 => $row['phone'],
            'delivery_type'         => $row['delivery_type'],
            'carrier_code'          => $row['carriercode'],
            'express_company'       => $row['express_company'],
            'promotion_amount'      => $row['promotion_amount'],
            'coupon_amount'         => $row['coupon_amount'],
            'pay_sub_total'         => $row['paysubtotal'],
            'taxation_amount'       => $row['taxation_amount'],
            'self_delivery_id'      => $row['self_delivery_id']
        ]);
    }
}
