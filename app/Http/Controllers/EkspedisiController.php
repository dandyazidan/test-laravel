<?php

namespace App\Http\Controllers;

use App\Ekspedisi;
use Illuminate\Http\Request;
use App\Imports\EkspedisiImport;
use Illuminate\Support\Facades\DB;
use Excel;

class EkspedisiController extends Controller
{
    public function storeData(Request $request)
    {
        //VALIDASI
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        $data_orders = DB::table('orders')->select('order_id')->get();

        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new EkspedisiImport, $file); //IMPORT FILE 
            // DB::table('orders')->where('order_id', '==', 'ZQCS12-APM-1906102345')->update(['status_order' => 'Terkirim']);

            $orders = DB::table('ekspedisi')->select('order_id')->get();
            $users = DB::table('orders')
                ->join('ekspedisi', 'orders.order_id', '=', 'ekspedisi.order_id')
                // ->select('orders.order_id, orders.status_order')
                ->where('orders.order_id', '=', DB::raw('ekspedisi.order_id'))
                ->update(['status_order' => 'Diterima']);
            return redirect()->back()->with(['success' => 'Upload success']);
        }

        return redirect()->back()->with(['error' => 'Please choose file before']);
    }
}
