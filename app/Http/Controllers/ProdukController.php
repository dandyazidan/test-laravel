<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Produk;

class ProdukController extends Controller
{
    //
    public function getProduk()
    {
        $produk = DB::table('produk')->get();
        return view('admin.produk', ['produk' => $produk]);
    }

    public function addProduk()
    {
        return view('admin.add-produk');
    }

    public function saveProduk(Request $request)
    {
        $this->validate($request, [
            'sku_id' => 'required',
            'pop_sku_id' => 'required',
            'harga' => 'required'
        ]);

        Produk::create([
            'sku_id' => $request->sku_id,
            'pop_sku_id' => $request->pop_sku_id,
            'harga' => $request->harga,
        ]);

        return redirect()->route('produk')->with(['success' => 'Success add new produk']);
    }
    public function editProduk()
    {
        # code...
    }
    public function updateProduk()
    {
        # code...
    }
    public function hapusProduk()
    {
        # code...
    }
}
