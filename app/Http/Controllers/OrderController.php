<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;

class OrderController extends Controller
{
    public function getOrder()
    {
        $orders = DB::table('orders')->get();
        // dd($orders);
        return view('admin.order', ['orders' => $orders]);
    }

    public function getAddOrder()
    {
        $produk = DB::table('produk')->get();
        return view('admin.add-order', ['produk' => $produk]);
    }

    public function addOrder(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required',
            'sku_id' => 'required',
            'pop_sku_id' => 'required',
            'quantity' => 'required',
            'pay_type' => 'required',
            'total_price' => 'required',
            'customer_name' => 'required',
            'province' => 'required',
            'city' => 'required',
            'district' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'delivery_type' => 'required',
            'pay_sub_total' => 'required',

        ]);

        Order::create([
            'order_id' => $request->order_id,
            'sku_id' => $request->sku_id,
            'pop_sku_id' => $request->pop_sku_id,
            'quantity' => $request->quantity,
            'pay_type' => $request->pay_type,
            'booktime' => $request->booktime,
            'total_price' => $request->total_price,
            'installment_fee' => $request->installment_fee,
            'order_state' => $request->order_state,
            'freight' => $request->freight,
            'customer_name' => $request->customer_name,
            'remark' => $request->remark,
            'delivery_time' => $request->delivery_time,
            'order_complete_time' => $request->order_complete_time,
            'province' => $request->province,
            'city' => $request->city,
            'district' => $request->district,
            'address' => $request->address,
            'phone' => $request->phone,
            'delivery_type' => $request->delivery_type,
            'carrier_code' => $request->carrier_code,
            'express_company' => $request->express_company,
            'promotion_amount' => $request->promotion_amount,
            'coupon_amount' => $request->coupon_amount,
            'pay_sub_total' => $request->pay_sub_total,
            'taxation_amount' => $request->taxation_amount,
            'self_delivery_id' => $request->self_delivery_id,
            'status_order' => 'Menunggu',
        ]);

        return redirect()->route('order')->with(['success' => 'Success add new orders']);
    }
}
